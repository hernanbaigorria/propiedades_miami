<?php get_header(); ?>
	<div class="col-12 residencial_fondo hidden-sm-down d-flex justify-content-center align-items-center mg-80">
		<div class="container">
			<div class="col-12 text-left">
				<h3>Residenciales</h3>
			</div>
		</div>
	</div>

	<div class="col-12 residencial_fondo d-flex hidden-md-up justify-content-center align-items-center mg-80" style="background:url('<?php bloginfo('template_url'); ?>/images/residenciales_iphone.jpg');">
		<div class="container">
			<div class="col-12 text-left">
				<h3>Residenciales</h3>
			</div>
		</div>
	</div>

	<section class="grilla">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-3 custom-filter">
					<div class="absolute-lupa">
						<img src="<?php bloginfo('template_url'); ?>/images/lupa.png">
					</div>
					<?php get_search_form() ?>
					<ul>
						<?php $post_type = 'residenciales';

						// Get all the taxonomies for this post type
						$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
						 
						foreach( $taxonomies as $taxonomy ) :
						 
						    // Gets every "category" (term) in this taxonomy to get the respective posts
						    $terms = get_terms( $taxonomy );
						 
						    foreach( $terms as $term ) :
						    	$link = get_term_link( $term);?>
						 	<li>
						      <label><a href="<?php echo $link ?>"><?php echo $term->name; ?></a></label>
						    </li>
						    <?php endforeach;
						 
						endforeach; ?>
					</ul>
				</div>
				<div class="col-12 col-md-8">
					<?php while ( have_posts() ) : the_post();
						$image = get_field('foto');?>
						<div class="col-12 title-grilla">
					        <h3><?php echo the_title(); ?></h3>
							<hr>
						</div>
						<img src="<?php echo $image['url']; ?>" class="img-fluid">
						<?php if( have_rows('informacion') ): ?>
        					<?php while( have_rows('informacion') ): the_row(); ?>
								<div class="col-12 title-info p-0">
									<h3><?php echo the_sub_field('precio') ?></h3>
									<hr>
								</div>
								<div class="col-12 info p-0">
									<h2><?php echo the_sub_field('direccion') ?></h2>
									<h4><?php echo the_sub_field('titlo_de_descripcion') ?></h4>
									<div class="info-int">
									<?php echo the_sub_field('descripcion') ?>
									</div>
									<h1>Ficha técnica</h1>
									<div class="ficha_tecnica"><?php echo the_sub_field('ficha_tecnica') ?></div>
									<h1>Galería</h1>
								</div>
							<?php endwhile; ?>
		    			<?php endif; ?>
		    			<div class="row m-15">
		    			<?php if( have_rows('galeria') ): ?>
        					<?php while( have_rows('galeria') ): the_row();
        						$galeriaimg = get_sub_field('imagen');?>
        						<div class="col-12 col-md-4">
	        						<a class="image-popup-vertical-fit" href="<?php echo $galeriaimg['url']; ?>">
	        							<div style="background:url('<?php echo $galeriaimg['url']; ?>');padding-bottom:65%;background-size:cover;background-position:center;margin-bottom:30px;">
	        								<img src="<?php echo $galeriaimg['url']; ?>" class="img-fluid" style="height:0;" />
	        							</div>
	        						</a>
        						</div>
        					<?php endwhile; ?>
		    			<?php endif; ?>
		    			</div>
					<?php endwhile; ?>
					<div class="col-12 text-right">
						<a href="<?php bloginfo('siteurl'); ?>/residenciales/?_sft_barrio=aventura" class="btn-home">Volver</a>
					</div>
				</div>
			</div>
		</div>
	</section>

<section class="contacto">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>contacto</h3>
				<hr>
			</div>
			<div class="col-12 col-sm-3">
				<p>Alejandro R. Silbestein PA <br>
				License 3376033 <br><br>

				2841 NE 185 street, unit 502 <br>
				Miami, FL 33180 <br>
				+1 (786) 356 9287 <br>
				<a href="maitlo:alejandro@mispropiedadesenmiami.com">alejandro@mispropiedadesenmiami.com</a></p>
			</div>
			<div class="col-12 col-sm-3"></div>
			<div class="col-12 col-sm-6">
				<form method="post" id="contact-form" action="<?php bloginfo('template_directory'); ?>/process.php" novalidate="novalidate">
			        <ol>

			            <li id="name-container" class="col-12">
			                <input type="text" name="name" id="name" placeholder="NOMBRE" <?php if (isset($errors['name'])) { echo 'class="error"';}?> value="" required="required"/>
			            </li>
			           
			            <li id="email-container" class="col-12">
			                <input type="email" name="email" id="email" placeholder="MAIL" <?php if (isset($errors['email'])) { echo 'class="error"';}?> value="<?php echo $email; ?>" required="required" />
			            </li>

			            <li id="phone-container" class="col-12">
			                <input type="text" name="phone" id="phone" placeholder="TELÉFONO" <?php if (isset($errors['phone'])) { echo 'class="error"';}?> value="<?php echo $phone; ?>" required="required" />
			            </li>

			            <li id="message-container" class="col-12">
			                <textarea rows="6" name="message" id="message" placeholder="CONSULTA" <?php if (isset($errors['message'])) { echo 'class="error"';}?> required="required"/><?php echo $message; ?></textarea>
			            </li>
			            <li id="submit-container" class="col-12">                            
			                <input class="button" type="submit" name="send" value="Enviar" id="send" />
			                <span id="loading"></span>                
			            </li>
			        </ol>
			        <div id="submit-message" class="col-12">
			            <span class="<?php echo (isset($formOK) ? $responsetype : 'hidden'); ?>"><?php if(isset($formOK)) { echo $submitmessage; } ?></span>
			        </div>
			    </form>
			</div>
		</div>
	</div>
</section>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js'></script>
<script type="text/javascript">
	$(document).ready(function(){
	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
	  mainClass: 'mfp-with-zoom', 
	  gallery:{
				enabled:true
			},

	  zoom: {
	    enabled: true, 

	    duration: 300, // duration of the effect, in milliseconds
	    easing: 'ease-in-out', // CSS transition easing function

	    opener: function(openerElement) {

	      return openerElement.is('img') ? openerElement : openerElement.find('img');
	  }
	}

	});

	});
</script>

<?php get_footer(); ?>		