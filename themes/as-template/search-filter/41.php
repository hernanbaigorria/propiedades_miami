
<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      http://www.designsandcode.com/
 * @copyright 2014 Designs & Code
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

if ( $query->have_posts() )
{
	?>

	<!--
	
	Found <?php echo $query->found_posts; ?> Results<br />
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />

	-->
	
	<?php
	while ($query->have_posts())
	{
		$query->the_post();
		$image = get_field('foto');
		
		?>
		<!--<div class="col-sm-3 col-xs-12">-->

		<div class="col-12 title-grilla">
			<?$terms = get_the_terms( get_the_ID(), 'barrio' );
	            foreach ( $terms as $term ) {
	              $term_id = $term->name;
	              $link = get_term_link( $term);
	              echo '<h3>'.$term_id.'</h3>';
	            }
          	?>
			<hr>
		</div>
		<div class="row m-left-15">
	        <div class="col-12 col-md-6 col-xl-4 p-r-0">
	    		<div class="index-item" style="background:url('<?php echo $image['url']; ?>');"></div>
	    		<div class="info-item text-center">
				<?php if( have_rows('informacion') ): ?>
        			<?php while( have_rows('informacion') ): the_row(); ?>
	    			<h3><?php echo the_title() ?></h3>
	    			<hr>
	    			<h2><?php echo the_sub_field('precio') ?></h2>
	    			<p><strong><?php echo the_sub_field('breve_descripcion') ?></strong></p>
	    			<h5><?php echo the_sub_field('titlo_de_descripcion') ?></h5>
        		   	<?php endwhile; ?>
		    	<?php endif; ?>
	    			<a href="<?php the_permalink(); ?>" class="btn-home">Conocé más</a>
	    		</div>
	    	</div>
    	</div>
		<!--
		<?$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		echo '<div class="products-index" style="background: url('. $url.')">';?>

		<div class="absolute-product">
			<i class="fa fa-heart-o" aria-hidden="true"></i>
			<?$terms = get_the_terms( get_the_ID(), 'categoria' );
	            foreach ( $terms as $term ) {
	              $term_id = $term->name;
	              $link = get_term_link( $term);
	              echo '<p>'.$term_id.'</p>';
	            }
          	?>
	          <h2><?php the_title(); ?></h2>

	        <a href="<?php the_permalink(); ?>">VER MÁS</a>
	     
		</div>-->
			
			<!--<p><br /><?php the_excerpt(); ?><p>-->
			
			<!--<p><?php the_category(); ?><p>-->
			<!--<p><?php the_tags(); ?>
			asd<p>-->
			<!--<p><small><?php the_date(); ?></small><p>-->
			<!--
		<?php echo"</div>";?>
		</div>
	-->
		<?php
	}
	?>

	<div class="col-lg-12">
	<!--
	Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />
	-->
	</div>
	<!--
	<div class="pagination">
		
		<div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
		<div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
		<?php
			/* example code for using the wp_pagenavi plugin */
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
	</div>
	-->
	<?php
}
else
{
	echo "<div class='col-12 no-result'><div class='col-12 title-grilla'><h3>No hubo resultados</h3><hr></div></div>";
}
?>