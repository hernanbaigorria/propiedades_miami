<?php 
/*
Template Name: Pages
*/
get_header(); ?>


	<?php if(is_page('sobre-mi')) { ?>

		<div class="col-12 p-0 portada_sobre_mi d-flex justify-content-center align-items-center mg-80">
			<div class="container">
				<div class="col-12 text-left">
					<h3>CONSULTORÍA PROFESIONAL INTEGRAL</h3>
				</div>
			</div>
		</div>
		<section class="contacto" style="background:#f9f9f9">
			<div class="container">
				<div class="row">
					<div class="col-12 hidden-md-up text-center">
						<img src="<?php bloginfo('template_url'); ?>/images/alejandro_perfil.png" class="img-fluid box-shadow" style="margin-bottom: 60px;">
					</div>
					<div class="col-12">
						<h3>SOBRE Mí</h3>
						<hr>
					</div>
					<div class="col-12 col-sm-6">
						<p>Luego de varios años trabajando en este sector, y ahora radicado en Miami, he obtenido mi licencia y en función a ello puedo asesorarte de manera personal y profesional al 100%, segun lo requiere la ley. <br><br>

						La seguridad jurídica del país, la transparencia y agilidad para llevar adelante los negocios, una economía completamente regida por las leyes de oferta y demanda del mercado, y por sobre todo, una ciudad en plena transformación, en vías de convertirse en Top Ten a nivel mundial, me llevan a creer firmemente en las grandes oportunidades que invertir aquí pueden generar. </p>
					</div>
					<div class="col-12 col-sm-1"></div>
					<div class="col-12 col-sm-5 d-flex justify-content-center align-items-center">
						<img src="<?php bloginfo('template_url'); ?>/images/sobre_mi_logos.png" class="img-fluid">
					</div>
				</div>
			</div>
		</section>

		<section class="contacto" style="background:#fff">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h3>contacto</h3>
						<hr>
					</div>
					<div class="col-12 col-md-5 col-xl-3">
						<p>Alejandro R. Silbestein PA <br>
						License 3376033 <br><br>

						2841 NE 185 street, unit 502 <br>
						Miami, FL 33180 <br>
						+1 (786) 356 9287 <br>
						<a href="maitlo:alejandro@mispropiedadesenmiami.com">alejandro@mispropiedadesenmiami.com</a></p>
					</div>
					<div class="col-12 col-md-1 col-xl-3"></div>
					<div class="col-12 col-md-6 col-xl-6">
						<form method="post" id="contact-form" action="<?php bloginfo('template_directory'); ?>/process.php" novalidate="novalidate">
					        <ol>

					            <li id="name-container" class="col-12">
					                <input type="text" name="name" id="name" placeholder="NOMBRE" <?php if (isset($errors['name'])) { echo 'class="error"';}?> value="" required="required"/>
					            </li>
					           
					            <li id="email-container" class="col-12">
					                <input type="email" name="email" id="email" placeholder="MAIL" <?php if (isset($errors['email'])) { echo 'class="error"';}?> value="<?php echo $email; ?>" required="required" />
					            </li>

					            <li id="phone-container" class="col-12">
					                <input type="text" name="phone" id="phone" placeholder="TELÉFONO" <?php if (isset($errors['phone'])) { echo 'class="error"';}?> value="<?php echo $phone; ?>" required="required" />
					            </li>

					            <li id="message-container" class="col-12">
					                <textarea rows="6" name="message" id="message" placeholder="CONSULTA" <?php if (isset($errors['message'])) { echo 'class="error"';}?> required="required"/><?php echo $message; ?></textarea>
					            </li>
					            <li id="submit-container" class="col-12">                            
					                <input class="button" type="submit" name="send" value="Enviar" id="send" />
					                <span id="loading"></span>                
					            </li>
					        </ol>
					        <div id="submit-message" class="col-12">
					            <span class="<?php echo (isset($formOK) ? $responsetype : 'hidden'); ?>"><?php if(isset($formOK)) { echo $submitmessage; } ?></span>
					        </div>
					    </form>
					</div>
				</div>
			</div>
		</section>

		<?php } elseif(is_page('propuesta')) { ?>

			<div class="col-12 propuesta_fondo hidden-sm-down d-flex justify-content-center align-items-center mg-80">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Productos a medida<br>de cada inversor</h3>
					</div>
				</div>
			</div>

			<div class="col-12 propuesta_fondo hidden-md-up d-flex justify-content-center align-items-center mg-80" style="background:url('<?php bloginfo('template_url'); ?>/images/propuesta_iphone.jpg');">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Productos a medida<br>de cada inversor</h3>
					</div>
				</div>
			</div>

			<section class="propuesta" style="background:#f9f9f9">
				<div class="container">
					<div class="row">
						<div class="col-12 col-sm-6">
							
							<h3>Asesoramiento en todo el proceso de Compra, Administración y Venta de Propiedades</h3>
							<p>Llevar adelante, y mantener, una inversión a mas de 8.000 km de distancia es un desafio. Por ello tener in situ al profesional adecuado, de confianza, que te guie en todo el proceso es un punto clave, y alli es donde contas conmigo.
							Una incorrecta selección de propiedades, o aun una adecuada, pero manejada erróneamente se puede convertir en una mala inversión.</p>

							
						</div>
						<div class="col-12 col-sm-6">
							<h3>Pasos previos a la compra</h3>
							<p style="margin:0">Antes de invertir, es necesario tener en claro todos los aspectos legales / societarios que te permitan proteger tu inversión y patrimonio de la mejor manera posible. En esa dirección, te puedo ayudar a tomar la mejor decisión.</p>
						</div>
					</div>
				</div>
			</section>

			<section class="propiedades-info" style="background:#f9f9f9">
				<div class="container">
					<div class="row align-items-center">
						
						<div class="col-12 col-sm-6 text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/icon_home_01.png" class="img-fluid">
						</div>
						<div class="col-12 col-sm-6 text-right">
							<h3>Selección de Propiedades</h3>
							<hr>
							<p>Las propiedades se ofrecen en un mercado único y transparente, donde se encuentra abundante información disponible, por lo cual es preciso una correcta lectura e interpretacion de la misma. Realizando análisis comparativos de mercado, contrastando los precios de venta históricos, y las unidades disponibles a la fecha, para asi arribar al precio justo de mercado. En este punto crítico de toda inversión (la selección del producto sobre el cual invertir) tambien te puedo asesorar. 

							<br><br>En función al perfil y objetivo de cada inversor (uso personal, renta, residencial, comercial, monto de la inversión, etc.) se determinará el tipo de producto a buscar en el mercado, y en base a ello seleccionaré las mejores alternativas que se adapten a ese objetivo.</p>
						</div>
					</div>
				</div>
			</section>

			<section class="propiedades-info">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-12 col-sm-6 text-center hidden-md-up">
							<img src="<?php bloginfo('template_url'); ?>/images/icon_home_02.png" class="img-fluid">
						</div>
						<div class="col-12 col-sm-6">
							
							<h3>Administración <br> de propiedades</h3>
							<hr>
							<p>Para proteger e incrementar el valor de tu patrimonio, no solo es importante una buena compra, tambien es crucial la adecuada gestión de tus activos, y alli también puedo ayudarte.</p>
						</div>
						<div class="col-12 col-sm-6 text-center hidden-sm-down">
							<img src="<?php bloginfo('template_url'); ?>/images/icon_home_02.png" class="img-fluid">
						</div>
					</div>
				</div>
			</section>

			<section class="propiedades-info" style="background:#f9f9f9;">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-12 col-sm-6 text-center">
							<img src="<?php bloginfo('template_url'); ?>/images/icon_home_03.png" class="img-fluid">
						</div>
						<div class="col-12 col-sm-6 text-right">
							<h3>compra-venta<br>de propiedades</h3>
							<hr>
							<p style="font-weight:600;">Cerrando todo proceso de inversión, sigue la venta. Para ello utilizare todas las herramientas disponibles en pos de asesorarte sobre el mejor momento de salida, y asegurarte el mejor precio posible del mercado.</p>
							<h2>Aspectos legales-impositivos</h2>
							<p style="font-weight:600;">Una adecualda planificación legal e impositiva en la que puedo asesorarte a fin de generar el máximo rendimiento de la inversión.</p>
						</div>
					</div>
				</div>
			</section>

			<section class="contacto" style="background: #fff;">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h3>contacto</h3>
							<hr>
						</div>
						<div class="col-12 col-md-5 col-xl-3">
							<p>Alejandro R. Silbestein PA <br>
							License 3376033 <br><br>

							2841 NE 185 street, unit 502 <br>
							Miami, FL 33180 <br>
							+1 (786) 356 9287 <br>
							<a href="maitlo:alejandro@mispropiedadesenmiami.com">alejandro@mispropiedadesenmiami.com</a></p>
						</div>
						<div class="col-12 col-md-1 col-xl-3"></div>
						<div class="col-12 col-md-6 col-xl-6">
							<form method="post" id="contact-form" action="<?php bloginfo('template_directory'); ?>/process.php" novalidate="novalidate">
						        <ol>

						            <li id="name-container" class="col-12">
						                <input type="text" name="name" id="name" placeholder="NOMBRE" <?php if (isset($errors['name'])) { echo 'class="error"';}?> value="" required="required"/>
						            </li>
						           
						            <li id="email-container" class="col-12">
						                <input type="email" name="email" id="email" placeholder="MAIL" <?php if (isset($errors['email'])) { echo 'class="error"';}?> value="<?php echo $email; ?>" required="required" />
						            </li>

						            <li id="phone-container" class="col-12">
						                <input type="text" name="phone" id="phone" placeholder="TELÉFONO" <?php if (isset($errors['phone'])) { echo 'class="error"';}?> value="<?php echo $phone; ?>" required="required" />
						            </li>

						            <li id="message-container" class="col-12">
						                <textarea rows="6" name="message" id="message" placeholder="CONSULTA" <?php if (isset($errors['message'])) { echo 'class="error"';}?> required="required"/><?php echo $message; ?></textarea>
						            </li>
						            <li id="submit-container" class="col-12">                            
						                <input class="button" type="submit" name="send" value="Enviar" id="send" />
						                <span id="loading"></span>                
						            </li>
						        </ol>
						        <div id="submit-message" class="col-12">
						            <span class="<?php echo (isset($formOK) ? $responsetype : 'hidden'); ?>"><?php if(isset($formOK)) { echo $submitmessage; } ?></span>
						        </div>
						    </form>
						</div>
					</div>
				</div>
			</section>

		<?php } elseif(is_page('comerciales')) { ?>

			<div class="col-12 comerciales_fondo hidden-sm-down d-flex justify-content-center align-items-center mg-80">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Negocios a medida</h3>
					</div>
				</div>
			</div>

			<div class="col-12 comerciales_fondo hidden-md-up d-flex justify-content-center align-items-center mg-80" style="background:url('<?php bloginfo('template_url'); ?>/images/comerciales_iphone.jpg');">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Negocios a medida</h3>
					</div>
				</div>
			</div>

		<section class="comerciales" style="background:#fff">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6">
						<h3>formato de la inversión</h3>
						<hr>
						<h4>opción 1</h4>
						<p>Compra de la plaza comercial / edificio por parte de inversor individual mediate su sociedad (LLC).</p>
						<h4>opción 2 - formato pool</h4>
						<p>Dado que este tipo de inversiones involucra montos importantes (por arriba de los 2 millones de dolares) te propongo la alternativa de estructurar el negocio en este formato. Bajo este esquema, el inversor aporta dinero y adquiere una cuota parte del total de la inversion, diversificando así el riesgo, al convertirte en propietario de un porcentaje de un Edificio Comercial, y no de un local u oficina en particular, lo que asegurara un ingreso permanente de rentas (al ser dueño de un % y no de un local particular, si un local queda vacante, de todas maneras tendrá ingresos proporcionales por las rentas de los otros).</p>
					</div>
					<div class="col-12 col-sm-6">
						<h3>beneficios</h3>
						<hr>
						<p>Podemos estructurar la inversión de acuerdo a los objetivos puntuales de cada inversor. Contactame y conversaremos sobre cual es la mejor estrategia en base a tu horizonte y objetivos de inversion. <br><br>
						</p>
						<ul>
							<li><p>Retornos más estables y con menos riesgos. </p></li>
							<li><p>Contratos de alquiler mas extensos (de 3 a 5 e incluso algunos 10 años). </p></li>
							<li><p>Menor impacto de las comisiones de alquiler. </p></li>
							<li><p>Menores costos de mantenimiento, y gran parte de los mismo asumidos por los inquilinos. </p></li>
							<li><p>Diversificación del riesgo. </p></li>
						</ul>
					</div>
					
				</div>
			</div>
		</section>


		<section class="middle-box d-flex justify-content-center align-items-center">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6">
						<h3>inversión llave en mano</h3>
						<hr>
						<p>Mi propuesta de negocios abarca todos los aspectos necesarios para llevar adelante este tipo de inversiones de manera ordenada y segura desde todos los puntos de vista (legal, impositivo, y economico). Esto implica todos los pasos, tanto sea desde la creación de la sociedad, pasando por la búsqueda y selección de propiedades, análisis de las mismas, negociación de la compra, inspección y due diligence hasta la posterior administración de la misma. Es decir, una inversión llave en mano.</p>
					</div>
					<div class="col-12 col-sm-1"></div>
					<div class="col-12 col-sm-5 d-flex justify-content-center align-items-center">
					</div>
				</div>
			</div>
		</section>

		<section class="casos_de_exito">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h3>casos de éxito</h3>
						<hr>
					</div>
					<?php 
					    $args = array(
					        'post_type' => 'comerciales',
					    );
					    query_posts($args);
					    //The Loop
					    while ( have_posts() ) : the_post();
					    	$image = get_field('foto');?>
							<div class="col-12 info-comercial">
								<h4><?php the_title() ?></h4>
								<?php if( have_rows('informacion') ): ?>
				        			<?php while( have_rows('informacion') ): the_row(); ?>
									<img src="<?php echo $image['url']; ?>" class="img-fluid">
									<h3><?php echo the_sub_field('precio') ?></h3>
									<h1><?php echo the_sub_field('subtitulo') ?></h1>
									<h2><?php echo the_sub_field('descripcion') ?></h2>
									<p><strong>Fecha de compra:</strong> <?php echo the_sub_field('fecha_de_compra') ?> <br>
									<?php echo the_sub_field('direccion') ?></p>
									<hr>
				        		   	<?php endwhile; ?>
						    	<?php endif; ?>
							</div>
					<?php endwhile; ?>
					
				</div>
			</div>
		</section>

		<section class="contacto">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h3>contacto</h3>
						<hr>
					</div>
					<div class="col-12 col-md-5 col-xl-3">
						<p>Alejandro R. Silbestein PA <br>
						License 3376033 <br><br>

						2841 NE 185 street, unit 502 <br>
						Miami, FL 33180 <br>
						+1 (786) 356 9287 <br>
						<a href="maitlo:alejandro@mispropiedadesenmiami.com">alejandro@mispropiedadesenmiami.com</a></p>
					</div>
					<div class="col-12 col-md-1 col-xl-3"></div>
					<div class="col-12 col-md-6 col-xl-6">
						<form method="post" id="contact-form" action="<?php bloginfo('template_directory'); ?>/process.php" novalidate="novalidate">
					        <ol>

					            <li id="name-container" class="col-12">
					                <input type="text" name="name" id="name" placeholder="NOMBRE" <?php if (isset($errors['name'])) { echo 'class="error"';}?> value="" required="required"/>
					            </li>
					           
					            <li id="email-container" class="col-12">
					                <input type="email" name="email" id="email" placeholder="MAIL" <?php if (isset($errors['email'])) { echo 'class="error"';}?> value="<?php echo $email; ?>" required="required" />
					            </li>

					            <li id="phone-container" class="col-12">
					                <input type="text" name="phone" id="phone" placeholder="TELÉFONO" <?php if (isset($errors['phone'])) { echo 'class="error"';}?> value="<?php echo $phone; ?>" required="required" />
					            </li>

					            <li id="message-container" class="col-12">
					                <textarea rows="6" name="message" id="message" placeholder="CONSULTA" <?php if (isset($errors['message'])) { echo 'class="error"';}?> required="required"/><?php echo $message; ?></textarea>
					            </li>
					            <li id="submit-container" class="col-12">                            
					                <input class="button" type="submit" name="send" value="Enviar" id="send" />
					                <span id="loading"></span>                
					            </li>
					        </ol>
					        <div id="submit-message" class="col-12">
					            <span class="<?php echo (isset($formOK) ? $responsetype : 'hidden'); ?>"><?php if(isset($formOK)) { echo $submitmessage; } ?></span>
					        </div>
					    </form>
					</div>
				</div>
			</div>
		</section>

		<?php } elseif(is_page('residenciales')) { ?>

			<div class="col-12 residencial_fondo d-flex hidden-sm-down justify-content-center align-items-center mg-80">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Residenciales</h3>
					</div>
				</div>
			</div>

			<div class="col-12 residencial_fondo d-flex hidden-md-up justify-content-center align-items-center mg-80" style="background:url('<?php bloginfo('template_url'); ?>/images/residenciales_iphone.jpg');">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Residenciales</h3>
					</div>
				</div>
			</div>

			<section class="grilla">
				<div class="container">
					<div class="row">
						<div class="col-12 col-md-3 custom-filter">
							<div class="absolute-lupa">
								<img src="<?php bloginfo('template_url'); ?>/images/lupa.png">
							</div>
							<?php get_search_form() ?>
							<ul>
								<?php $post_type = 'residenciales';
	 
								// Get all the taxonomies for this post type
								$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
								 
								foreach( $taxonomies as $taxonomy ) :
								 
								    // Gets every "category" (term) in this taxonomy to get the respective posts
								    $terms = get_terms( $taxonomy );
								 
								    foreach( $terms as $term ) :
								    	$link = get_term_link( $term);?>
								 	<li>
								      <label><a href="<?php echo $link ?>"><?php echo $term->name; ?></a></label>
								    </li>
								    <?php endforeach;
								 
								endforeach; ?>
							</ul>
						</div>
						<div class="col-12 col-md-8">
							<?php $post_type = 'residenciales';
 
								// Get all the taxonomies for this post type
								$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
								 
								foreach( $taxonomies as $taxonomy ) :
								 
								    // Gets every "category" (term) in this taxonomy to get the respective posts
								    $terms = get_terms( $taxonomy );
								 
								    foreach( $terms as $term ) : ?>
								 	<div class="col-12 title-grilla">
								      <h3><?php echo $term->name; ?></h3>
								      <hr>
								    </div>

								 
								        <?php
								        $args = array(
								                'post_type' => $post_type,
								                'posts_per_page' => -1,  //show all posts
								                'tax_query' => array(
								                    array(
								                        'taxonomy' => $taxonomy,
								                        'field' => 'slug',
								                        'terms' => $term->slug,
								                    )
								                )
								 
								            );
								        $posts = new WP_Query($args);?>
								 		<div class="row m-left-15"><?
								        if( $posts->have_posts() ): while( $posts->have_posts() ) : $posts->the_post();
								        	$image = get_field('foto');?>
								 
								                    <?php if(has_post_thumbnail()) { ?>
								                            <?php the_post_thumbnail(); ?>
								                    <?php }
								                    /* no post image so show a default img */
								                    else { ?>
								                           
								                    <?php } ?>
								 
								                   
									        <div class="col-12 col-md-6 col-xl-4 p-r-0">
									    		<div class="index-item" style="background:url('<?php echo $image['url']; ?>');"></div>
									    		<div class="info-item text-center">
												<?php if( have_rows('informacion') ): ?>
								        			<?php while( have_rows('informacion') ): the_row(); ?>
									    			<h3><?php echo the_title() ?></h3>
									    			<hr>
									    			<h2><?php echo the_sub_field('precio') ?></h2>
									    			<p><strong><?php echo the_sub_field('breve_descripcion') ?></strong></p>
									    			<h5><?php echo the_sub_field('titlo_de_descripcion') ?></h5>
								        		   	<?php endwhile; ?>
										    	<?php endif; ?>
									    			<a href="<?php the_permalink(); ?>" class="btn-home">Conoce más</a>
									    		</div>
									    	</div>
								    	
								                   
								 
								        <?php endwhile; endif; ?>
								 	</div>
								    <?php endforeach;
							 
							endforeach; ?>
						</div>
					</div>
				</div>
			</section>
		
		<?php } elseif(is_page('contacto')) { ?>


		<div class="col-12 p-0 portada_contacto hidden-sm-down d-flex justify-content-center align-items-center mg-80">
			<div class="container">
				<div class="col-12 text-left">
					<h3>No dudes en <br>consultarme</h3>
				</div>
			</div>
		</div>

		<div class="col-12 p-0 portada_contacto hidden-md-up d-flex justify-content-center align-items-center mg-80" style="background:url('<?php bloginfo('template_url'); ?>/images/contacto_iphone.jpg');">
			<div class="container">
				<div class="col-12 text-left">
					<h3>No dudes en <br>consultarme</h3>
				</div>
			</div>
		</div>

		<section class="contacto" style="background:#fff">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h3>contacto</h3>
						<hr>
					</div>
					<div class="col-12 col-md-5 col-xl-3">
						<p>Alejandro R. Silbestein PA <br>
						License 3376033 <br><br>

						2841 NE 185 street, unit 502 <br>
						Miami, FL 33180 <br>
						+1 (786) 356 9287 <br>
						<a href="maitlo:alejandro@mispropiedadesenmiami.com">alejandro@mispropiedadesenmiami.com</a></p>
					</div>
					<div class="col-12 col-md-1 col-xl-3"></div>
					<div class="col-12 col-md-6 col-xl-6">
						<form method="post" id="contact-form" action="<?php bloginfo('template_directory'); ?>/process.php" novalidate="novalidate">
					        <ol>

					            <li id="name-container" class="col-12">
					                <input type="text" name="name" id="name" placeholder="NOMBRE" <?php if (isset($errors['name'])) { echo 'class="error"';}?> value="" required="required"/>
					            </li>
					           
					            <li id="email-container" class="col-12">
					                <input type="email" name="email" id="email" placeholder="MAIL" <?php if (isset($errors['email'])) { echo 'class="error"';}?> value="<?php echo $email; ?>" required="required" />
					            </li>

					            <li id="phone-container" class="col-12">
					                <input type="text" name="phone" id="phone" placeholder="TELÉFONO" <?php if (isset($errors['phone'])) { echo 'class="error"';}?> value="<?php echo $phone; ?>" required="required" />
					            </li>

					            <li id="message-container" class="col-12">
					                <textarea rows="6" name="message" id="message" placeholder="CONSULTA" <?php if (isset($errors['message'])) { echo 'class="error"';}?> required="required"/><?php echo $message; ?></textarea>
					            </li>
					            <li id="submit-container" class="col-12">                            
					                <input class="button" type="submit" name="send" value="Enviar" id="send" />
					                <span id="loading"></span>                
					            </li>
					        </ol>
					        <div id="submit-message" class="col-12">
					            <span class="<?php echo (isset($formOK) ? $responsetype : 'hidden'); ?>"><?php if(isset($formOK)) { echo $submitmessage; } ?></span>
					        </div>
					    </form>
					</div>
				</div>
			</div>
		</section>
		<section class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3587.624123727411!2d-80.14412958562566!3d25.94756390721659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9acf8ba6f5377%3A0x5c1b0ce7965a69d2!2s2841+NE+185th+St+APT+502%2C+Aventura%2C+FL+33180%2C+EE.+UU.!5e0!3m2!1ses!2sar!4v1535227133864" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			<div class="layer"></div>
		</section>
		
	<?php } ?>

	
<?php get_footer(); ?>