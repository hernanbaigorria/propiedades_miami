<?php get_header(); ?>
		<div class="col-12 residencial_fondo d-flex hidden-sm-down justify-content-center align-items-center mg-80">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Residenciales</h3>
					</div>
				</div>
			</div>

			<div class="col-12 residencial_fondo d-flex hidden-md-up justify-content-center align-items-center mg-80" style="background:url('<?php bloginfo('template_url'); ?>/images/residenciales_iphone.jpg');">
				<div class="container">
					<div class="col-12 text-left">
						<h3>Residenciales</h3>
					</div>
				</div>
			</div>

			<section class="grilla">
				<div class="container">
					<div class="row">
						<div class="col-12 col-md-3 custom-filter">
							<div class="absolute-lupa">
								<img src="<?php bloginfo('template_url'); ?>/images/lupa.png">
							</div>
							<?php get_search_form() ?>
							<ul>
								<?php $post_type = 'residenciales';
	 
								// Get all the taxonomies for this post type
								$taxonomies = get_object_taxonomies( array( 'post_type' => $post_type ) );
								 
								foreach( $taxonomies as $taxonomy ) :
								 
								    // Gets every "category" (term) in this taxonomy to get the respective posts
								    $terms = get_terms( $taxonomy );
								 
								    foreach( $terms as $term ) :
								    	$link = get_term_link( $term);?>
								 	<li>
								      <label><a href="<?php echo $link ?>"><?php echo $term->name; ?></a></label>
								    </li>
								    <?php endforeach;
								 
								endforeach; ?>
							</ul>
							
						</div>
						<div class="col-12 col-md-8">
							<div class="col-12 title-grilla">
								
								<?$terms = get_the_terms( get_the_ID(), 'barrio' );
										if (!empty($terms)):
							            foreach ( $terms as $term ) {
							              $term_id = $term->name;
							              $link = get_term_link( $term);
							              echo '<h3>'.$term_id.'</h3>';
							            }
							            endif;
						          	?>
					          	<hr>
					          	
							</div>
							<div class="row m-left-15">
							<?php if(have_posts()) : ?>
							   <?php while(have_posts()) : the_post(); 
							   	$image = get_field('foto');?>
							
								<div class="col-12 col-md-6 col-xl-4 p-r-0">
						    		<div class="index-item" style="background:url('<?php echo $image['url']; ?>');"></div>
						    		<div class="info-item text-center">
									<?php if( have_rows('informacion') ): ?>
					        			<?php while( have_rows('informacion') ): the_row(); ?>
						    			<h3><?php echo the_title() ?></h3>
						    			<hr>
						    			<h2><?php echo the_sub_field('precio') ?></h2>
						    			<p><strong><?php echo the_sub_field('breve_descripcion') ?></strong></p>
						    			<h5><?php echo the_sub_field('titlo_de_descripcion') ?></h5>
					        		   	<?php endwhile; ?>
							    	<?php endif; ?>
						    			<a href="<?php the_permalink(); ?>" class="btn-home">Conoce más</a>
						    		</div>
						    	</div>
								 
							 	<?php the_content(); ?>
							
							   <?php endwhile; ?>
							</div>
							
							<?php else : ?>
							
							<div class='col-12 no-result' style="position: absolute;top: 0;background: #fff;"><div class='col-12 title-grilla'><h3>No hubo resultados</h3><hr></div></div>
							
							<?php endif; ?>
						</div>
					</div>
				</div>
			</section>
	



<?php get_footer(); ?>