$(function() {
    
    // Form validation via plugin
    var submitMessage     = $('#submit-message'),
        messageContainer  = submitMessage.find('span'),
        loading           = $('#loading');

    var submitMessageNewsletter     = $('#submit-message-n'),
        messageContainer  = submitMessageNewsletter.find('span'),
        loading           = $('#loading-n');
        
    function showMessage(message, classAttr) {
        messageContainer.text(message)
        messageContainer.attr('class', classAttr);
    }

    function showMessageNewsletter(message, classAttr) {
        messageContainer.text(message)
        messageContainer.attr('class', classAttr);
    }
    
        
    $('#contact-form').validate({        
               
        // Override to submit the form via ajax
        submitHandler: function(form) {
            var options = {
                beforeSubmit: function() {
                    loading.show();
                },
                success: function() {
                    showMessage('Gracias! Su mensaje fue enviado con éxito.', 'success');
                    form.reset();
                    loading.hide();
                },
                error: function() {
                    showMessage('Hubo un problema con el envio, intente mas tarde.', 'failure');
                    loading.hide();
                }
            };
            $(form).ajaxSubmit(options);
        },
        invalidHandler: function() {
            showMessage('Compruebe todos los campos.', 'failure');
        }
    });
    $('#newsletter-form').validate({        
               
        // Override to submit the form via ajax
        submitHandler: function(form) {
            var options = {
                beforeSubmit: function() {
                    loading.show();
                },
                success: function() {
                    showMessageNewsletter('Gracias! Su mensaje fue enviado con éxito.', 'success');
                    form.reset();
                    loading.hide();
                },
                error: function() {
                    showMessageNewsletter('Hubo un problema con el envio, intente mas tarde.', 'failure');
                    loading.hide();
                }
            };
            $(form).ajaxSubmit(options);
        },
        invalidHandler: function() {
            showMessageNewsletter('Compruebe todos los campos.', 'failure');
        }
    });
});
