<?php 
/*
Template Name: Front
*/
get_header(); ?>

<div id="carouselExampleIndicators" class="carousel slide mg-80 hidden-sm-down" data-ride="carousel">
  
  <div class="carousel-inner">

    <div class="carousel-item active">
      <div class="slider-content d-flex justify-content-center align-items-center">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-4 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>Soluciones a medida</h3>
		      		<hr>
		      		<p>Asesoramiento en todo el proceso de compra, administración y venta de propiedades</p>
		      	</div>
		      	<div class="col-12 col-md-8"></div>
	      	</div>
      	</div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content d-flex justify-content-center align-items-center" style="background:url('<?php bloginfo('template_url'); ?>/images/Slider_home_02.jpg');">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-4 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>SELECCIÓN DE PROPIEDADES</h3>
  		      		<hr>
  		      		<p>El punto crítico de toda inversión es la selección del producto sobre el cual invertir</p>
		      	</div>
	      	</div>
      	</div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content d-flex justify-content-center align-items-center" style="background:url('<?php bloginfo('template_url'); ?>/images/Slider_home_03.jpg');">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-4 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>ADMINISTRACIÓN DE PROPIEDADES</h3>
  		      		<hr>
  		      		<p>Para proteger e incrementar el valor de tu patrimonio es crucial la adecuada gestión de tus activos</p>
		      	</div>
	      	</div>
      	</div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content d-flex justify-content-center align-items-center" style="background:url('<?php bloginfo('template_url'); ?>/images/Slider_home_04.jpg');">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-6 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>COMPRA-VENTA DE PROPIEDADES</h3>
  		      		<hr>
  		      		<p>Consultoría sobre el momento más oportuno de salida, para asegurar el mejor precio del mercado</p>
		      	</div>
	      	</div>
      	</div>
      </div>
    </div>
    <!--
    <div class="carousel-item">
      <div class="slider-content">
      	<div class="text-absulote d-flex justify-content-center align-items-center flex-column">
      		<h3>SELECCIÓN DE PROPIEDADES</h3>
      		<hr>
      		<p>El punto crítico de toda inversión es la selección del producto sobre el cual invertir</p>
      	</div>
      	<img src="<?php bloginfo('template_url'); ?>/images/Slider_text_01.png" class="img-fluid max-w-rectangle">
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content">
      	<div class="text-absulote d-flex justify-content-center align-items-center flex-column">
      		<h3>ADMINISTRACIÓN DE PROPIEDADES</h3>
      		<hr>
      		<p>Para proteger e incrementar el valor de tu patrimonio es crucial la adecuada gestión de tus activos</p>
      	</div>
      	<img src="<?php bloginfo('template_url'); ?>/images/Slider_text_01.png" class="img-fluid max-w-rectangle">
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content">
      	<div class="text-absulote d-flex justify-content-center align-items-center flex-column">
      		<h3>COMPRA-VENTA DE PROPIEDADES</h3>
      		<hr>
      		<p>Consultoría sobre el momento más oportuno de salida, para asegurar el mejor precio del mercado</p>
      	</div>
      	<img src="<?php bloginfo('template_url'); ?>/images/Slider_text_01.png" class="img-fluid max-w-rectangle">
      </div>
    </div>
	-->
	<div class="container">
		<div class="row">
		  	<ol class="carousel-indicators">
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		  	</ol>
	  	</div>
  	</div>
  </div>
</div>

<div id="carouselExampleIndicators" class="carousel slide mg-80 hidden-md-up" data-ride="carousel">
  
  <div class="carousel-inner">

    <div class="carousel-item active">
      <div class="slider-content d-flex justify-content-center align-items-center" style="background:url('<?php bloginfo('template_url'); ?>/images/slider_iphone_01.jpg');">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-4 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>Soluciones a medida</h3>
		      		<hr>
		      		<p>Asesoramiento en todo el proceso de compra, administración y venta de propiedades</p>
		      	</div>
		      	<div class="col-12 col-md-8"></div>
	      	</div>
      	</div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content d-flex justify-content-center align-items-center" style="background:url('<?php bloginfo('template_url'); ?>/images/slider_iphone_02.jpg');">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-4 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>SELECCIÓN DE PROPIEDADES</h3>
  		      		<hr>
  		      		<p>El punto crítico de toda inversión es la selección del producto sobre el cual invertir</p>
		      	</div>
	      	</div>
      	</div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content d-flex justify-content-center align-items-center" style="background:url('<?php bloginfo('template_url'); ?>/images/slider_iphone_03.jpg');">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-4 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>ADMINISTRACIÓN DE PROPIEDADES</h3>
  		      		<hr>
  		      		<p>Para proteger e incrementar el valor de tu patrimonio es crucial la adecuada gestión de tus activos</p>
		      	</div>
	      	</div>
      	</div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="slider-content d-flex justify-content-center align-items-center" style="background:url('<?php bloginfo('template_url'); ?>/images/slider_iphone_04.jpg');">
      	<div class="container">
      		<div class="row">
		      	<div class="col-12 col-md-6 text-absulote d-flex justify-content-center align-items-center flex-column">
		      		<h3>COMPRA-VENTA DE PROPIEDADES</h3>
  		      		<hr>
  		      		<p>Consultoría sobre el momento más oportuno de salida, para asegurar el mejor precio del mercado</p>
		      	</div>
	      	</div>
      	</div>
      </div>
    </div>
	<div class="container">
		<div class="row">
		  	<ol class="carousel-indicators">
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
		  	  <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		  	</ol>
	  	</div>
  	</div>
  </div>
</div>

<section class="sobre_mi">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-4">
				<img src="<?php bloginfo('template_url'); ?>/images/alejandro_perfil.png" class="img-fluid box-shadow">
			</div>
			<div class="col-12 col-md-1"></div>
			<div class="col-12 col-md-6">
				<h3>SOBRE Mí</h3>
				<hr>
				<p>Soy un profesional de las ciencias economicas, MBA, formado en el mundo de las finanzas corporativas, que decidió re-orientarse hacia el asesoramiento y administración de inversiones de Real Estate en Florida. </p>
				<img src="<?php bloginfo('template_url'); ?>/images/logos_home.png" class="img-fluid" style="display:block;">
				<a href="<?php bloginfo('siteurl'); ?>/sobre-mi" class="btn-home">Conoce más</a>
			</div>
			<div class="col-12 col-md-1"></div>
		</div>
	</div>
</section>

<section class="parallax">
	<img src="<?php bloginfo('template_url'); ?>/images/parralax_home.jpg" class="img-fluid hidden-sm-down">
	<img src="<?php bloginfo('template_url'); ?>/images/banner_iphone.jpg" class="img-fluid hidden-md-up">
</section>

<section class="icons">
	<div class="container">
		<div class="row">
			<div class="col-12 col-xl-1 hidden-lg-down"></div>
			<div class="col-12 col-md-4 col-xl-2 text-center p-0">
				<img src="<?php bloginfo('template_url'); ?>/images/icon_home_01.png" class="img-fluid">
				<h2>Selección de propiedades</h2>
			</div>
			<div class="col-12 col-xl-2 hidden-lg-down"></div>
			<div class="col-12 col-md-4 col-xl-2 text-center p-0">
				<img src="<?php bloginfo('template_url'); ?>/images/icon_home_02.png" class="img-fluid">
				<h2>administración de propiedades</h2>
			</div>
			<div class="col-12 col-xl-2 hidden-lg-down"></div>
			<div class="col-12 col-md-4 col-xl-2 text-center p-0">
				<img src="<?php bloginfo('template_url'); ?>/images/icon_home_03.png" class="img-fluid">
				<h2>compra-venta de propiedades</h2>
			</div>
			<div class="col-12 col-xl-1 hidden-lg-down"></div>
			<div class="col-12 text-center">
				<a href="<?php bloginfo('siteurl'); ?>/propuesta" class="btn-home">ver más</a>
			</div>
		</div>
	</div>
</section>

<section class="propiedades">
	<div class="row m-0">
		<a href="<?php bloginfo('siteurl'); ?>/residenciales/?_sft_barrio=aventura" class="col-12 col-sm-6 p-0 hover-propiedades">
			<div class="propiedad_back_01 d-flex justify-content-center align-items-center">
				<h3 class="d-flex justify-content-center align-items-center">Residenciales</h3>
			</div>
		</a>
		<a href="<?php bloginfo('siteurl'); ?>/comerciales/" class="col-12 col-sm-6 p-0 hover-propiedades">
			<div class="propiedad_back_02 d-flex justify-content-center align-items-center">
				<h3 class="d-flex justify-content-center align-items-center">Comerciales</h3>
			</div>
		</a>
	</div>
</section>

<div id="experiencias" class="carousel slide slider-content-exp d-flex justify-content-center align-items-center flex-wrap hidden-sm-down" data-ride="carousel">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 p-0">
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Comencé tímidamente con las inversiones en Fl con Alejandro, dado q era algo nuevo para mi que quería salir del riesgo de mi país. Desde hace 3 años a la fecha he incrementado 4 veces las inversiones, con eso te digo todo.” <br><br>Daniel, Argentina.</p>
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="carousel-item">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Es un profesional en todo el sentido de la palabra. Me ayudo desde lo legal, hasta incluso a abrir una cuenta en banco. Yo me olvidaba de temas impositivos, o legales, y el siempre me recordaba y completaba todo! Las propiedades son tal cual me lo informo, y los retornos son lo esperado, a veces un poco mas, a veces un poco menos, pero dentro del promedio buscado.” <br><br>Mariela J. - Argentina.</p>
				      		
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="carousel-item">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Maneja mis propiedades comerciales y residenciales, y a la fecha, desde hace 4 anios, todo ha sido impecable. 
								Se encarga de todo, y me informa de todo. Es como un ejecutivo de cuentas del banco que te maneja las inversiones y te rinde las cuentas cada mes.” <br><br>Jorge G. - Chile.</p>
				      		
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="carousel-item">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Yo tenia inversiones en USA con otra gente.
								Me lo presento un amigo, y cambie todo mi portafolio con Alejandro.
								No es un vendedor, es un profesional analizando y buscando el mejor negocio posible para el cliente.” <br><br>Gaston P. - Argentina.</p>
				      		
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="arrows">
      			  <a class="carousel-control-prev" href="#experiencias" role="button" data-slide="prev">
      			    <i class="fas fa-angle-left"></i>
      			  </a>
      			  <a class="carousel-control-next" href="#experiencias" role="button" data-slide="next">
      			    <i class="fas fa-angle-right"></i>
      			  </a>
	      		</div>
			  </div>
			</div>
		</div>
	</div>
</div>
<div id="experiencias_responsive" class="carousel slide slider-content-exp d-flex justify-content-center align-items-center flex-wrap hidden-md-up" style="background:url('<?php bloginfo('template_url'); ?>/images/slider_iphone_03.jpg');" data-ride="carousel">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 p-0">
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Comencé tímidamente con las inversiones en Fl con Alejandro, dado q era algo nuevo para mi que quería salir del riesgo de mi país. Desde hace 3 años a la fecha he incrementado 4 veces las inversiones, con eso te digo todo.” <br><br>Daniel, Argentina.</p>
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="carousel-item">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Es un profesional en todo el sentido de la palabra. Me ayudo desde lo legal, hasta incluso a abrir una cuenta en banco. Yo me olvidaba de temas impositivos, o legales, y el siempre me recordaba y completaba todo! Las propiedades son tal cual me lo informo, y los retornos son lo esperado, a veces un poco mas, a veces un poco menos, pero dentro del promedio buscado.” <br><br>Mariela J. - Argentina.</p>
				      		
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="carousel-item">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Maneja mis propiedades comerciales y residenciales, y a la fecha, desde hace 4 anios, todo ha sido impecable. 
								Se encarga de todo, y me informa de todo. Es como un ejecutivo de cuentas del banco que te maneja las inversiones y te rinde las cuentas cada mes.” <br><br>Jorge G. - Chile.</p>
				      		
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="carousel-item">
			      <div class="d-flex justify-content-center align-items-center flex-wrap">
			      	<div class="container">
				      	<div class="col-12 col-md-6 p-0">
				      		<h3>Experiencias</h3>
				      		<hr>
				      		<p>“Yo tenia inversiones en USA con otra gente.
								Me lo presento un amigo, y cambie todo mi portafolio con Alejandro.
								No es un vendedor, es un profesional analizando y buscando el mejor negocio posible para el cliente.” <br><br>Gaston P. - Argentina.</p>
				      		
				      	</div>
			      	</div>
			      </div>
			    </div>
			    <div class="arrows">
      			  <a class="carousel-control-prev" href="#experiencias_responsive" role="button" data-slide="prev">
      			    <i class="fas fa-angle-left"></i>
      			  </a>
      			  <a class="carousel-control-next" href="#experiencias_responsive" role="button" data-slide="next">
      			    <i class="fas fa-angle-right"></i>
      			  </a>
	      		</div>
			  </div>
			</div>
		</div>
	</div>
</div>
<section class="contacto">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>contacto</h3>
				<hr>
			</div>
			<div class="col-12 col-md-5 col-xl-3">
				<p>Alejandro R. Silbestein PA <br>
				License 3376033 <br><br>

				2841 NE 185 street, unit 502 <br>
				Miami, FL 33180 <br>
				+1 (786) 356 9287 <br>
				<a href="maitlo:alejandro@mispropiedadesenmiami.com">alejandro@mispropiedadesenmiami.com</a></p>
			</div>
			<div class="col-12 col-md-1 col-xl-3"></div>
			<div class="col-12 col-md-6 col-xl-6">
				<form method="post" id="contact-form" action="<?php bloginfo('template_directory'); ?>/process.php" novalidate="novalidate">
			        <ol>

			            <li id="name-container" class="col-12">
			                <input type="text" name="name" id="name" placeholder="NOMBRE" <?php if (isset($errors['name'])) { echo 'class="error"';}?> value="" required="required"/>
			            </li>
			           
			            <li id="email-container" class="col-12">
			                <input type="email" name="email" id="email" placeholder="MAIL" <?php if (isset($errors['email'])) { echo 'class="error"';}?> value="<?php echo $email; ?>" required="required" />
			            </li>

			            <li id="phone-container" class="col-12">
			                <input type="text" name="phone" id="phone" placeholder="TELÉFONO" <?php if (isset($errors['phone'])) { echo 'class="error"';}?> value="<?php echo $phone; ?>" required="required" />
			            </li>

			            <li id="message-container" class="col-12">
			                <textarea rows="6" name="message" id="message" placeholder="CONSULTA" <?php if (isset($errors['message'])) { echo 'class="error"';}?> required="required"/><?php echo $message; ?></textarea>
			            </li>
			            <li id="submit-container" class="col-12">                            
			                <input class="button" type="submit" name="send" value="Enviar" id="send" />
			                <span id="loading"></span>                
			            </li>
			        </ol>
			        <div id="submit-message" class="col-12">
			            <span class="<?php echo (isset($formOK) ? $responsetype : 'hidden'); ?>"><?php if(isset($formOK)) { echo $submitmessage; } ?></span>
			        </div>
			    </form>
			</div>
		</div>
	</div>
</section>

<section class="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3587.624123727411!2d-80.14412958562566!3d25.94756390721659!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88d9acf8ba6f5377%3A0x5c1b0ce7965a69d2!2s2841+NE+185th+St+APT+502%2C+Aventura%2C+FL+33180%2C+EE.+UU.!5e0!3m2!1ses!2sar!4v1535227133864" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	<div class="layer"></div>
</section>


<section class="newsletter" style="background: #3fa6a6">
	<form method="post" id="newsletter-form" action="<?php bloginfo('template_directory'); ?>/process_newsletter.php" novalidate="novalidate">
        <ol>
        	<div class="container">
	        	<div class="row">
	        		<div class="col-12 col-md-1"></div>
		          	<div class="col-12 col-md-4">
		          		<h3>¡Recibe toda la información! Suscríbete</h3>
		          	</div>
		            <li id="email-container" class="col-12 col-md-4">
		                <input type="email" name="email" id="email" placeholder="E-MAIL" <?php if (isset($errors['email'])) { echo 'class="error"';}?> value="<?php echo $email; ?>" required="required" />
		            	<div id="submit-message-n" class="col-12 p-0" style="clear:both;">
		                    <span class="<?php echo (isset($formOK) ? $responsetype : 'hidden'); ?>"><?php if(isset($formOK)) { echo $submitmessage; } ?></span>
		                </div> 
		            </li>

		            <li id="submit-container" class="col-12 col-md-2">                            
		                <input class="button" type="submit" name="send" value="Enviar" id="send" />
		                <span id="loading-n"></span>   

		            </li>
		            <div class="col-12 col-md-1"></div>
	            </div>
            </div>
        </ol>
        
    </form>
</section>
<?php get_footer(); ?>