-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 04-09-2018 a las 05:48:31
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `propiedades_miami`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Un comentarista de WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-08-22 23:42:24', '2018-08-23 02:42:24', 'Hola, esto es un comentario.\nPara empezar a moderar, editar y borrar comentarios, por favor, visitá la pantalla de comentarios en el escritorio.\nLos avatares de los comentaristas provienen de <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://propiedades.test', 'yes'),
(2, 'home', 'http://propiedades.test', 'yes'),
(3, 'blogname', 'Mis propiedades en Miami', 'yes'),
(4, 'blogdescription', 'Otro sitio realizado con WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'hernanebaigorria@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'j F, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:116:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"/residenciales/?$\";s:33:\"index.php?post_type=residenciales\";s:47:\"/residenciales/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_type=residenciales&feed=$matches[1]\";s:42:\"/residenciales/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_type=residenciales&feed=$matches[1]\";s:34:\"/residenciales/page/([0-9]{1,})/?$\";s:51:\"index.php?post_type=residenciales&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:41:\"residenciales/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"residenciales/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"residenciales/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"residenciales/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"residenciales/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"residenciales/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"residenciales/([^/]+)/embed/?$\";s:46:\"index.php?residenciales=$matches[1]&embed=true\";s:34:\"residenciales/([^/]+)/trackback/?$\";s:40:\"index.php?residenciales=$matches[1]&tb=1\";s:54:\"residenciales/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?residenciales=$matches[1]&feed=$matches[2]\";s:49:\"residenciales/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?residenciales=$matches[1]&feed=$matches[2]\";s:42:\"residenciales/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?residenciales=$matches[1]&paged=$matches[2]\";s:49:\"residenciales/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?residenciales=$matches[1]&cpage=$matches[2]\";s:38:\"residenciales/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?residenciales=$matches[1]&page=$matches[2]\";s:30:\"residenciales/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"residenciales/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"residenciales/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"residenciales/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"residenciales/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"residenciales/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:47:\"barrio/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?barrio=$matches[1]&feed=$matches[2]\";s:42:\"barrio/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?barrio=$matches[1]&feed=$matches[2]\";s:23:\"barrio/([^/]+)/embed/?$\";s:39:\"index.php?barrio=$matches[1]&embed=true\";s:35:\"barrio/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?barrio=$matches[1]&paged=$matches[2]\";s:17:\"barrio/([^/]+)/?$\";s:28:\"index.php?barrio=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=17&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:45:\"acf-flexible-content/acf-flexible-content.php\";i:1;s:27:\"acf-gallery/acf-gallery.php\";i:2;s:37:\"acf-options-page/acf-options-page.php\";i:3;s:29:\"acf-repeater/acf-repeater.php\";i:4;s:30:\"advanced-custom-fields/acf.php\";i:5;s:39:\"search-filter-pro/search-filter-pro.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '-3', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'as-template', 'yes'),
(41, 'stylesheet', 'as-template', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(84, 'page_on_front', '17', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'es_AR', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'nonce_key', 'oG:v11[8x$4Ms9zF%IQoZ1UX.?X[P4(4ep].g#IZ =`;D_gfpm}FwF`$7V(Mi}]4', 'no'),
(108, 'nonce_salt', '#genGHc![Mm1dOQ5e+rc<5dsGqA,oJ~D8  0^ZP FYTL=#n8B@06%}Ky)@:</BS)', 'no'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1536043513;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1536072147;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1536107986;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1536115360;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1534992783;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(123, 'auth_key', 'R_vvu6Y(/ 5L]wpEk? _qd4:VZv%ydWg~}<w8zYi&F=::Rkog@Yk7qqiMCDBQdZC', 'no'),
(124, 'auth_salt', ')@wb]hfsph@@1EmlNQ0]O_9-(aiZK^gbQz@#p|t*MC P!J1(!zEi8h~.dG4]OHak', 'no'),
(125, 'logged_in_key', 'yHKs5XHv4#YhmW][Ha5|2>zCCX663mb?~i)Pd&dzSA*GviA{1=r=@Lu>c#V9+jj,', 'no'),
(126, 'logged_in_salt', 'Q1)kO`1Gqkw@?>t/u5r2HVKEsGNLFM_WCd%R?D=?Uo+>LapvpYG#s>tZE@n*|I^w', 'no'),
(134, 'can_compress_scripts', '1', 'no'),
(146, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/es_AR/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"es_AR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/es_AR/wordpress-4.9.8.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1536030076;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}', 'no'),
(147, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:26:\"hernanebaigorria@gmail.com\";s:7:\"version\";s:5:\"4.9.8\";s:9:\"timestamp\";i:1534992332;}', 'no'),
(148, 'current_theme', 'Custom', 'yes'),
(149, 'theme_mods_as-template', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(150, 'theme_switched', '', 'yes'),
(160, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(167, 'page_for_posts', '0', 'yes'),
(201, '_site_transient_timeout_browser_01fe6d96f512df15cc1b10345d6b37d9', '1536421800', 'no'),
(202, '_site_transient_browser_01fe6d96f512df15cc1b10345d6b37d9', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"68.0.3440.106\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(212, 'recently_activated', 'a:0:{}', 'yes'),
(216, 'acf_version', '5.7.2', 'yes'),
(228, 'widget_search_filter_register_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(233, 'barrio_children', 'a:0:{}', 'yes'),
(262, '_transient_timeout_acf_plugin_updates', '1536104089', 'no'),
(263, '_transient_acf_plugin_updates', 'a:3:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:86400;s:6:\"status\";i:1;}', 'no'),
(269, '_site_transient_timeout_theme_roots', '1536031880', 'no'),
(270, '_site_transient_theme_roots', 'a:4:{s:11:\"as-template\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(271, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1536030083;s:7:\"checked\";a:4:{s:11:\"as-template\";s:3:\"2.0\";s:13:\"twentyfifteen\";s:3:\"1.9\";s:15:\"twentyseventeen\";s:3:\"1.5\";s:13:\"twentysixteen\";s:3:\"1.4\";}s:8:\"response\";a:3:{s:13:\"twentyfifteen\";a:4:{s:5:\"theme\";s:13:\"twentyfifteen\";s:11:\"new_version\";s:3:\"2.0\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentyfifteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentyfifteen.2.0.zip\";}s:15:\"twentyseventeen\";a:4:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"1.7\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.1.7.zip\";}s:13:\"twentysixteen\";a:4:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.1.5.zip\";}}s:12:\"translations\";a:0:{}}', 'no'),
(272, '_transient_timeout_acf_early_access_info', '1536033685', 'no'),
(273, '_transient_acf_early_access_info', 'a:3:{s:7:\"version\";s:6:\"4.4.12\";s:8:\"versions\";a:118:{i:0;s:5:\"trunk\";i:1;s:5:\"5.7.2\";i:2;s:5:\"5.7.1\";i:3;s:5:\"5.7.0\";i:4;s:5:\"5.6.9\";i:5;s:5:\"5.6.8\";i:6;s:5:\"5.6.7\";i:7;s:5:\"5.6.6\";i:8;s:5:\"5.6.5\";i:9;s:5:\"5.6.4\";i:10;s:5:\"5.6.3\";i:11;s:5:\"5.6.2\";i:12;s:6:\"5.6.10\";i:13;s:5:\"4.4.9\";i:14;s:5:\"4.4.8\";i:15;s:5:\"4.4.7\";i:16;s:5:\"4.4.6\";i:17;s:5:\"4.4.5\";i:18;s:5:\"4.4.4\";i:19;s:5:\"4.4.3\";i:20;s:5:\"4.4.2\";i:21;s:6:\"4.4.12\";i:22;s:6:\"4.4.11\";i:23;s:6:\"4.4.10\";i:24;s:5:\"4.4.1\";i:25;s:5:\"4.4.0\";i:26;s:5:\"4.3.9\";i:27;s:5:\"4.3.8\";i:28;s:5:\"4.3.7\";i:29;s:5:\"4.3.6\";i:30;s:5:\"4.3.5\";i:31;s:5:\"4.3.4\";i:32;s:5:\"4.3.3\";i:33;s:5:\"4.3.2\";i:34;s:5:\"4.3.1\";i:35;s:5:\"4.3.0\";i:36;s:5:\"4.2.2\";i:37;s:5:\"4.2.1\";i:38;s:5:\"4.2.0\";i:39;s:5:\"4.1.8\";i:40;s:5:\"4.1.6\";i:41;s:5:\"4.1.5\";i:42;s:5:\"4.1.4\";i:43;s:5:\"4.1.3\";i:44;s:5:\"4.1.2\";i:45;s:5:\"4.1.1\";i:46;s:5:\"4.1.0\";i:47;s:5:\"4.0.3\";i:48;s:5:\"4.0.2\";i:49;s:5:\"4.0.1\";i:50;s:5:\"4.0.0\";i:51;s:5:\"3.5.8\";i:52;s:5:\"3.5.7\";i:53;s:5:\"3.5.6\";i:54;s:5:\"3.5.5\";i:55;s:5:\"3.5.4\";i:56;s:5:\"3.5.3\";i:57;s:5:\"3.5.2\";i:58;s:5:\"3.5.1\";i:59;s:5:\"3.5.0\";i:60;s:5:\"3.4.3\";i:61;s:5:\"3.4.2\";i:62;s:5:\"3.4.1\";i:63;s:5:\"3.4.0\";i:64;s:5:\"3.3.9\";i:65;s:5:\"3.3.8\";i:66;s:5:\"3.3.7\";i:67;s:5:\"3.3.6\";i:68;s:5:\"3.3.5\";i:69;s:5:\"3.3.4\";i:70;s:5:\"3.3.3\";i:71;s:5:\"3.3.2\";i:72;s:5:\"3.3.1\";i:73;s:5:\"3.3.0\";i:74;s:5:\"3.2.9\";i:75;s:5:\"3.2.8\";i:76;s:5:\"3.2.7\";i:77;s:5:\"3.2.6\";i:78;s:5:\"3.2.5\";i:79;s:5:\"3.2.4\";i:80;s:5:\"3.2.3\";i:81;s:5:\"3.2.2\";i:82;s:5:\"3.2.0\";i:83;s:5:\"3.1.9\";i:84;s:5:\"3.1.8\";i:85;s:5:\"3.1.7\";i:86;s:5:\"3.1.6\";i:87;s:5:\"3.1.5\";i:88;s:5:\"3.1.4\";i:89;s:5:\"3.1.3\";i:90;s:5:\"3.1.2\";i:91;s:5:\"3.1.1\";i:92;s:5:\"3.1.0\";i:93;s:5:\"3.0.7\";i:94;s:5:\"3.0.6\";i:95;s:5:\"3.0.4\";i:96;s:5:\"3.0.3\";i:97;s:5:\"3.0.2\";i:98;s:5:\"3.0.1\";i:99;s:5:\"3.0.0\";i:100;s:5:\"2.1.4\";i:101;s:5:\"2.1.3\";i:102;s:5:\"2.1.1\";i:103;s:5:\"2.0.5\";i:104;s:5:\"2.0.4\";i:105;s:5:\"2.0.3\";i:106;s:5:\"2.0.2\";i:107;s:5:\"2.0.1\";i:108;s:5:\"2.0.0\";i:109;s:5:\"1.1.4\";i:110;s:5:\"1.1.3\";i:111;s:5:\"1.1.2\";i:112;s:5:\"1.1.1\";i:113;s:5:\"1.1.0\";i:114;s:5:\"1.0.5\";i:115;s:5:\"1.0.3\";i:116;s:5:\"1.0.2\";i:117;s:5:\"1.0.0\";}s:6:\"tested\";s:5:\"4.9.9\";}', 'no'),
(274, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1536030462;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1535071230:1'),
(4, 4, '_wp_page_template', 'tmp-pages.php'),
(5, 6, '_edit_last', '1'),
(6, 6, '_edit_lock', '1535071265:1'),
(7, 6, '_wp_page_template', 'tmp-pages.php'),
(8, 8, '_edit_last', '1'),
(9, 8, '_edit_lock', '1535071280:1'),
(10, 8, '_wp_page_template', 'tmp-pages.php'),
(11, 10, '_edit_last', '1'),
(12, 10, '_edit_lock', '1535071293:1'),
(13, 10, '_wp_page_template', 'tmp-pages.php'),
(16, 13, '_menu_item_type', 'post_type'),
(17, 13, '_menu_item_menu_item_parent', '0'),
(18, 13, '_menu_item_object_id', '4'),
(19, 13, '_menu_item_object', 'page'),
(20, 13, '_menu_item_target', ''),
(21, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(22, 13, '_menu_item_xfn', ''),
(23, 13, '_menu_item_url', ''),
(25, 14, '_menu_item_type', 'post_type'),
(26, 14, '_menu_item_menu_item_parent', '0'),
(27, 14, '_menu_item_object_id', '6'),
(28, 14, '_menu_item_object', 'page'),
(29, 14, '_menu_item_target', ''),
(30, 14, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(31, 14, '_menu_item_xfn', ''),
(32, 14, '_menu_item_url', ''),
(43, 16, '_menu_item_type', 'post_type'),
(44, 16, '_menu_item_menu_item_parent', '0'),
(45, 16, '_menu_item_object_id', '10'),
(46, 16, '_menu_item_object', 'page'),
(47, 16, '_menu_item_target', ''),
(48, 16, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(49, 16, '_menu_item_xfn', ''),
(50, 16, '_menu_item_url', ''),
(51, 17, '_edit_last', '1'),
(52, 17, '_edit_lock', '1535206466:1'),
(53, 17, '_wp_page_template', 'tmp-front.php'),
(54, 20, '_edit_last', '1'),
(55, 20, '_edit_lock', '1535816935:1'),
(56, 20, '_wp_page_template', 'tmp-pages.php'),
(57, 22, '_edit_last', '1'),
(58, 22, '_edit_lock', '1535818349:1'),
(59, 22, '_wp_page_template', 'tmp-pages.php'),
(60, 25, '_edit_last', '1'),
(61, 25, '_edit_lock', '1536021086:1'),
(62, 36, '_edit_last', '1'),
(63, 36, '_edit_lock', '1535918376:1'),
(64, 40, '_wp_attached_file', '2018/09/residenciales_01.jpg'),
(65, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:869;s:6:\"height\";i:580;s:4:\"file\";s:28:\"2018/09/residenciales_01.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"residenciales_01-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"residenciales_01-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"residenciales_01-768x513.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:513;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:16:\"img-lookbook-min\";a:4:{s:4:\"file\";s:28:\"residenciales_01-265x397.jpg\";s:5:\"width\";i:265;s:6:\"height\";i:397;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(66, 39, '_edit_last', '1'),
(67, 39, 'foto', '40'),
(68, 39, '_foto', 'field_5b8c3e9f9c4b4'),
(69, 39, 'informacion_0_nombre', ''),
(70, 39, '_informacion_0_nombre', 'field_5b8c3dc29c4ac'),
(71, 39, 'informacion_0_precio', '$2,695,000'),
(72, 39, '_informacion_0_precio', 'field_5b8c3dd89c4ad'),
(73, 39, 'informacion_0_direccion', '3300 NE 188TH ST, Aventura, FL 33180 Edificio ECHO Aventura, APT UPH15'),
(74, 39, '_informacion_0_direccion', 'field_5b8c3de29c4ae'),
(75, 39, 'informacion_0_titlo_de_descripcion', 'Extensa terraza y vista al agua'),
(76, 39, '_informacion_0_titlo_de_descripcion', 'field_5b8c3def9c4af'),
(77, 39, 'informacion_0_descripcion', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris porttitor aliquet arcu, in gravida nunc egestas eget. Quisque quis magna mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam tincidunt metus ante, sit amet elementum augue commodo ac. Quisque sollicitudin vel velit ut efficitur. Cras blandit arcu nulla, vitae consequat nibh scelerisque quis.'),
(78, 39, '_informacion_0_descripcion', 'field_5b8c3e1d9c4b0'),
(79, 39, 'informacion_0_ficha_tecnica', '<ul>\r\n 	<li>3 Bedrooms</li>\r\n 	<li>4 1/2 Baths</li>\r\n 	<li>SqFt: 2962 (275.2 mts2)</li>\r\n 	<li>Precio por SqFt: $943.62/s.f.</li>\r\n 	<li>4 1/2 Baths</li>\r\n 	<li>SqFt: 2962 (275.2 mts2)</li>\r\n 	<li>Precio por SqFt: $943.62/s.f.</li>\r\n</ul>'),
(80, 39, '_informacion_0_ficha_tecnica', 'field_5b8c3e629c4b1'),
(81, 39, 'informacion', '1'),
(82, 39, '_informacion', 'field_5b8c3db59c4ab'),
(83, 39, 'galeria', '4'),
(84, 39, '_galeria', 'field_5b8c3e809c4b2'),
(85, 39, '_edit_lock', '1536039964:1'),
(86, 41, '_edit_last', '1'),
(87, 41, '_edit_lock', '1536031275:1'),
(88, 41, '_search-filter-fields', 'a:2:{i:0;a:3:{s:4:\"type\";s:6:\"search\";s:7:\"heading\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}i:1;a:16:{s:4:\"type\";s:8:\"taxonomy\";s:10:\"input_type\";s:5:\"radio\";s:7:\"heading\";s:0:\"\";s:15:\"all_items_label\";s:0:\"\";s:10:\"show_count\";i:0;s:10:\"hide_empty\";i:0;s:12:\"hierarchical\";i:0;s:16:\"include_children\";i:1;s:10:\"drill_down\";i:0;s:20:\"sync_include_exclude\";i:1;s:9:\"combo_box\";i:0;s:8:\"operator\";s:2:\"or\";s:8:\"order_by\";s:7:\"default\";s:9:\"order_dir\";s:3:\"asc\";s:11:\"exclude_ids\";s:0:\"\";s:13:\"taxonomy_name\";s:6:\"barrio\";}}'),
(89, 41, '_search-filter-settings', 'a:23:{s:26:\"use_template_manual_toggle\";i:1;s:17:\"enable_auto_count\";i:0;s:20:\"template_name_manual\";s:0:\"\";s:9:\"page_slug\";s:0:\"\";s:10:\"post_types\";a:1:{s:13:\"residenciales\";i:1;}s:11:\"post_status\";a:1:{s:7:\"publish\";i:1;}s:15:\"use_ajax_toggle\";i:1;s:13:\"scroll_to_pos\";s:1:\"0\";s:16:\"custom_scroll_to\";s:0:\"\";s:11:\"auto_submit\";i:1;s:18:\"display_results_as\";s:9:\"shortcode\";s:15:\"update_ajax_url\";i:1;s:11:\"ajax_target\";s:8:\"#content\";s:11:\"results_url\";s:0:\"\";s:19:\"ajax_links_selector\";s:13:\".pagination a\";s:16:\"results_per_page\";i:10;s:16:\"exclude_post_ids\";s:0:\"\";s:17:\"taxonomy_relation\";s:0:\"\";s:15:\"default_sort_by\";s:1:\"0\";s:16:\"default_sort_dir\";s:4:\"desc\";s:16:\"default_meta_key\";s:10:\"_edit_last\";s:17:\"default_sort_type\";s:7:\"numeric\";s:19:\"taxonomies_settings\";a:4:{s:8:\"category\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:8:\"post_tag\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:11:\"post_format\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:6:\"barrio\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}}}'),
(90, 39, 'informacion_0_breve_descripcion', '3 Habitaciones - 4 1/2 Baños 275.2 m²'),
(91, 39, '_informacion_0_breve_descripcion', 'field_5b8c49b95e010'),
(101, 44, '_menu_item_type', 'custom'),
(102, 44, '_menu_item_menu_item_parent', '0'),
(103, 44, '_menu_item_object_id', '44'),
(104, 44, '_menu_item_object', 'custom'),
(105, 44, '_menu_item_target', ''),
(106, 44, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(107, 44, '_menu_item_xfn', ''),
(108, 44, '_menu_item_url', '#'),
(110, 39, 'galeria_0_imagen', '40'),
(111, 39, '_galeria_0_imagen', 'field_5b8c3e919c4b3'),
(112, 39, 'galeria_1_imagen', '40'),
(113, 39, '_galeria_1_imagen', 'field_5b8c3e919c4b3'),
(114, 39, 'galeria_2_imagen', '40'),
(115, 39, '_galeria_2_imagen', 'field_5b8c3e919c4b3'),
(116, 39, 'galeria_3_imagen', '40'),
(117, 39, '_galeria_3_imagen', 'field_5b8c3e919c4b3'),
(118, 45, '_edit_last', '1'),
(119, 45, '_edit_lock', '1536031246:1'),
(120, 45, '_search-filter-fields', 'a:2:{i:0;a:3:{s:4:\"type\";s:6:\"search\";s:7:\"heading\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}i:1;a:16:{s:4:\"type\";s:8:\"taxonomy\";s:10:\"input_type\";s:6:\"select\";s:7:\"heading\";s:0:\"\";s:15:\"all_items_label\";s:0:\"\";s:10:\"show_count\";i:0;s:10:\"hide_empty\";i:0;s:12:\"hierarchical\";i:0;s:16:\"include_children\";i:1;s:10:\"drill_down\";i:0;s:20:\"sync_include_exclude\";i:1;s:9:\"combo_box\";i:0;s:8:\"operator\";s:2:\"or\";s:8:\"order_by\";s:7:\"default\";s:9:\"order_dir\";s:3:\"asc\";s:11:\"exclude_ids\";s:0:\"\";s:13:\"taxonomy_name\";s:6:\"barrio\";}}'),
(121, 45, '_search-filter-settings', 'a:23:{s:26:\"use_template_manual_toggle\";i:1;s:17:\"enable_auto_count\";i:0;s:20:\"template_name_manual\";s:13:\"tmp-pages.php\";s:9:\"page_slug\";s:13:\"residenciales\";s:10:\"post_types\";a:1:{s:13:\"residenciales\";i:1;}s:11:\"post_status\";a:1:{s:7:\"publish\";i:1;}s:15:\"use_ajax_toggle\";i:1;s:13:\"scroll_to_pos\";s:1:\"0\";s:16:\"custom_scroll_to\";s:0:\"\";s:11:\"auto_submit\";i:1;s:18:\"display_results_as\";s:7:\"archive\";s:15:\"update_ajax_url\";i:1;s:11:\"ajax_target\";s:8:\"#content\";s:11:\"results_url\";s:14:\"/residenciales\";s:19:\"ajax_links_selector\";s:13:\".pagination a\";s:16:\"results_per_page\";i:10;s:16:\"exclude_post_ids\";s:0:\"\";s:17:\"taxonomy_relation\";s:0:\"\";s:15:\"default_sort_by\";s:1:\"0\";s:16:\"default_sort_dir\";s:4:\"desc\";s:16:\"default_meta_key\";s:10:\"_edit_last\";s:17:\"default_sort_type\";s:7:\"numeric\";s:19:\"taxonomies_settings\";a:4:{s:8:\"category\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:8:\"post_tag\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:11:\"post_format\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}s:6:\"barrio\";a:2:{s:15:\"include_exclude\";s:7:\"include\";s:3:\"ids\";s:0:\"\";}}}'),
(125, 45, '_wp_trash_meta_status', 'publish'),
(126, 45, '_wp_trash_meta_time', '1536031401'),
(127, 45, '_wp_desired_post_slug', 'residenciales-2'),
(128, 46, '_menu_item_type', 'post_type'),
(129, 46, '_menu_item_menu_item_parent', '44'),
(130, 46, '_menu_item_object_id', '22'),
(131, 46, '_menu_item_object', 'page'),
(132, 46, '_menu_item_target', ''),
(133, 46, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(134, 46, '_menu_item_xfn', ''),
(135, 46, '_menu_item_url', ''),
(137, 47, '_menu_item_type', 'post_type'),
(138, 47, '_menu_item_menu_item_parent', '44'),
(139, 47, '_menu_item_object_id', '20'),
(140, 47, '_menu_item_object', 'page'),
(141, 47, '_menu_item_target', ''),
(142, 47, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(143, 47, '_menu_item_xfn', ''),
(144, 47, '_menu_item_url', ''),
(146, 39, '_wp_old_slug', 'borrador-automatico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-08-22 23:42:24', '2018-08-23 02:42:24', 'Bienvenido a WordPress. Esta es tu primera entrada. Editala o borrala, ¡y comenzá a escribir!', '¡Hola mundo!', '', 'publish', 'open', 'open', '', 'hola-mundo', '', '', '2018-08-22 23:42:24', '2018-08-23 02:42:24', '', 0, 'http://propiedades.test/?p=1', 0, 'post', '', 1),
(4, 1, '2018-08-23 21:42:41', '2018-08-24 00:42:41', '', 'Sobre Mí', '', 'publish', 'closed', 'closed', '', 'sobre-mi', '', '', '2018-08-23 21:42:41', '2018-08-24 00:42:41', '', 0, 'http://propiedades.test/?page_id=4', 0, 'page', '', 0),
(5, 1, '2018-08-23 21:42:41', '2018-08-24 00:42:41', '', 'Sobre Mí', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2018-08-23 21:42:41', '2018-08-24 00:42:41', '', 4, 'http://propiedades.test/2018/08/23/4-revision-v1/', 0, 'revision', '', 0),
(6, 1, '2018-08-23 21:43:21', '2018-08-24 00:43:21', '', 'Propuesta', '', 'publish', 'closed', 'closed', '', 'propuesta', '', '', '2018-08-23 21:43:21', '2018-08-24 00:43:21', '', 0, 'http://propiedades.test/?page_id=6', 0, 'page', '', 0),
(7, 1, '2018-08-23 21:43:21', '2018-08-24 00:43:21', '', 'Propuesta', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2018-08-23 21:43:21', '2018-08-24 00:43:21', '', 6, 'http://propiedades.test/2018/08/23/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2018-08-23 21:43:38', '2018-08-24 00:43:38', '', 'Propiedades', '', 'publish', 'closed', 'closed', '', 'propiedades', '', '', '2018-08-23 21:43:38', '2018-08-24 00:43:38', '', 0, 'http://propiedades.test/?page_id=8', 0, 'page', '', 0),
(9, 1, '2018-08-23 21:43:38', '2018-08-24 00:43:38', '', 'Propiedades', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2018-08-23 21:43:38', '2018-08-24 00:43:38', '', 8, 'http://propiedades.test/2018/08/23/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2018-08-23 21:43:51', '2018-08-24 00:43:51', '', 'Contacto', '', 'publish', 'closed', 'closed', '', 'contacto', '', '', '2018-08-23 21:43:51', '2018-08-24 00:43:51', '', 0, 'http://propiedades.test/?page_id=10', 0, 'page', '', 0),
(11, 1, '2018-08-23 21:43:51', '2018-08-24 00:43:51', '', 'Contacto', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2018-08-23 21:43:51', '2018-08-24 00:43:51', '', 10, 'http://propiedades.test/2018/08/23/10-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2018-08-23 21:44:34', '2018-08-24 00:44:34', ' ', '', '', 'publish', 'closed', 'closed', '', '13', '', '', '2018-09-04 02:42:02', '2018-09-04 05:42:02', '', 0, 'http://propiedades.test/?p=13', 1, 'nav_menu_item', '', 0),
(14, 1, '2018-08-23 21:44:35', '2018-08-24 00:44:35', ' ', '', '', 'publish', 'closed', 'closed', '', '14', '', '', '2018-09-04 02:42:02', '2018-09-04 05:42:02', '', 0, 'http://propiedades.test/?p=14', 2, 'nav_menu_item', '', 0),
(16, 1, '2018-08-23 21:44:35', '2018-08-24 00:44:35', ' ', '', '', 'publish', 'closed', 'closed', '', '16', '', '', '2018-09-04 02:42:02', '2018-09-04 05:42:02', '', 0, 'http://propiedades.test/?p=16', 6, 'nav_menu_item', '', 0),
(17, 1, '2018-08-25 11:13:50', '2018-08-25 14:13:50', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-08-25 11:14:26', '2018-08-25 14:14:26', '', 0, 'http://propiedades.test/?page_id=17', 0, 'page', '', 0),
(18, 1, '2018-08-25 11:13:50', '2018-08-25 14:13:50', '', 'Home', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2018-08-25 11:13:50', '2018-08-25 14:13:50', '', 17, 'http://propiedades.test/2018/08/25/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2018-09-01 12:50:01', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-09-01 12:50:01', '0000-00-00 00:00:00', '', 0, 'http://propiedades.test/?p=19', 0, 'post', '', 0),
(20, 1, '2018-09-01 12:50:57', '2018-09-01 15:50:57', '', 'Comerciales', '', 'publish', 'closed', 'closed', '', 'comerciales', '', '', '2018-09-01 12:50:57', '2018-09-01 15:50:57', '', 0, 'http://propiedades.test/?page_id=20', 0, 'page', '', 0),
(21, 1, '2018-09-01 12:50:57', '2018-09-01 15:50:57', '', 'Comerciales', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2018-09-01 12:50:57', '2018-09-01 15:50:57', '', 20, 'http://propiedades.test/2018/09/01/20-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2018-09-01 12:58:04', '2018-09-01 15:58:04', '', 'Residenciales', '', 'publish', 'closed', 'closed', '', 'residenciales', '', '', '2018-09-01 12:58:04', '2018-09-01 15:58:04', '', 0, 'http://propiedades.test/?page_id=22', 0, 'page', '', 0),
(23, 1, '2018-09-01 12:58:04', '2018-09-01 15:58:04', '', 'Residenciales', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2018-09-01 12:58:04', '2018-09-01 15:58:04', '', 22, 'http://propiedades.test/2018/09/01/22-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2018-09-02 16:43:30', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-09-02 16:43:30', '0000-00-00 00:00:00', '', 0, 'http://propiedades.test/?post_type=residenciales&p=24', 0, 'residenciales', '', 0),
(25, 1, '2018-09-02 16:49:51', '2018-09-02 19:49:51', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"residenciales\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Residenciales', 'residenciales', 'publish', 'closed', 'closed', '', 'group_5b8c3d934b2cd', '', '', '2018-09-03 21:33:42', '2018-09-04 00:33:42', '', 0, 'http://propiedades.test/?post_type=acf-field-group&#038;p=25', 0, 'acf-field-group', '', 0),
(26, 1, '2018-09-02 16:49:51', '2018-09-02 19:49:51', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Foto', 'foto', 'publish', 'closed', 'closed', '', 'field_5b8c3e9f9c4b4', '', '', '2018-09-02 16:49:51', '2018-09-02 19:49:51', '', 25, 'http://propiedades.test/?post_type=acf-field&p=26', 0, 'acf-field', '', 0),
(27, 1, '2018-09-02 16:49:51', '2018-09-02 19:49:51', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";i:1;s:3:\"max\";i:1;s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Información', 'informacion', 'publish', 'closed', 'closed', '', 'field_5b8c3db59c4ab', '', '', '2018-09-02 16:51:10', '2018-09-02 19:51:10', '', 25, 'http://propiedades.test/?post_type=acf-field&#038;p=27', 1, 'acf-field', '', 0),
(29, 1, '2018-09-02 16:49:51', '2018-09-02 19:49:51', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Precio', 'precio', 'publish', 'closed', 'closed', '', 'field_5b8c3dd89c4ad', '', '', '2018-09-02 18:00:07', '2018-09-02 21:00:07', '', 27, 'http://propiedades.test/?post_type=acf-field&#038;p=29', 0, 'acf-field', '', 0),
(30, 1, '2018-09-02 16:49:51', '2018-09-02 19:49:51', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Dirección', 'direccion', 'publish', 'closed', 'closed', '', 'field_5b8c3de29c4ae', '', '', '2018-09-02 18:00:07', '2018-09-02 21:00:07', '', 27, 'http://propiedades.test/?post_type=acf-field&#038;p=30', 1, 'acf-field', '', 0),
(31, 1, '2018-09-02 16:49:51', '2018-09-02 19:49:51', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Titulo de descripción', 'titlo_de_descripcion', 'publish', 'closed', 'closed', '', 'field_5b8c3def9c4af', '', '', '2018-09-02 18:00:08', '2018-09-02 21:00:08', '', 27, 'http://propiedades.test/?post_type=acf-field&#038;p=31', 3, 'acf-field', '', 0),
(32, 1, '2018-09-02 16:49:52', '2018-09-02 19:49:52', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:5:\"basic\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Descripción completa', 'descripcion', 'publish', 'closed', 'closed', '', 'field_5b8c3e1d9c4b0', '', '', '2018-09-02 18:00:08', '2018-09-02 21:00:08', '', 27, 'http://propiedades.test/?post_type=acf-field&#038;p=32', 4, 'acf-field', '', 0),
(33, 1, '2018-09-02 16:49:52', '2018-09-02 19:49:52', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Ficha tecnica', 'ficha_tecnica', 'publish', 'closed', 'closed', '', 'field_5b8c3e629c4b1', '', '', '2018-09-02 18:00:08', '2018-09-02 21:00:08', '', 27, 'http://propiedades.test/?post_type=acf-field&#038;p=33', 5, 'acf-field', '', 0),
(34, 1, '2018-09-02 16:49:52', '2018-09-02 19:49:52', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:14:\"Agregar Imagen\";}', 'Galeria', 'galeria', 'publish', 'closed', 'closed', '', 'field_5b8c3e809c4b2', '', '', '2018-09-02 16:49:52', '2018-09-02 19:49:52', '', 25, 'http://propiedades.test/?post_type=acf-field&p=34', 2, 'acf-field', '', 0),
(35, 1, '2018-09-02 16:49:52', '2018-09-02 19:49:52', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Imagen', 'imagen', 'publish', 'closed', 'closed', '', 'field_5b8c3e919c4b3', '', '', '2018-09-03 21:33:42', '2018-09-04 00:33:42', '', 34, 'http://propiedades.test/?post_type=acf-field&#038;p=35', 0, 'acf-field', '', 0),
(36, 1, '2018-09-02 16:50:28', '2018-09-02 19:50:28', '', 'echo ph15', '', 'publish', 'closed', 'closed', '', 'echo-ph15', '', '', '2018-09-02 16:59:36', '2018-09-02 19:59:36', '', 0, 'http://propiedades.test/?post_type=residenciales&#038;p=36', 0, 'residenciales', '', 0),
(37, 1, '2018-09-02 16:50:39', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-09-02 16:50:39', '0000-00-00 00:00:00', '', 0, 'http://propiedades.test/?post_type=residenciales&p=37', 0, 'residenciales', '', 0),
(38, 1, '2018-09-02 16:51:14', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-09-02 16:51:14', '0000-00-00 00:00:00', '', 0, 'http://propiedades.test/?post_type=residenciales&p=38', 0, 'residenciales', '', 0),
(39, 1, '2018-09-02 16:54:04', '2018-09-02 19:54:04', '', 'echo ph22', '', 'publish', 'closed', 'closed', '', 'echo-ph22', '', '', '2018-09-04 02:46:04', '2018-09-04 05:46:04', '', 0, 'http://propiedades.test/?post_type=residenciales&#038;p=39', 0, 'residenciales', '', 0),
(40, 1, '2018-09-02 16:53:56', '2018-09-02 19:53:56', '', 'residenciales_01', '', 'inherit', 'open', 'closed', '', 'residenciales_01', '', '', '2018-09-02 16:53:56', '2018-09-02 19:53:56', '', 39, 'http://propiedades.test/wp-content/uploads/2018/09/residenciales_01.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2018-09-02 16:55:20', '2018-09-02 19:55:20', '', 'Residenciales', '', 'publish', 'closed', 'closed', '', 'residenciales', '', '', '2018-09-02 18:07:22', '2018-09-02 21:07:22', '', 0, 'http://propiedades.test/?post_type=search-filter-widget&#038;p=41', 0, 'search-filter-widget', '', 0),
(42, 1, '2018-09-02 17:36:31', '2018-09-02 20:36:31', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Breve Descripción', 'breve_descripcion', 'publish', 'closed', 'closed', '', 'field_5b8c49b95e010', '', '', '2018-09-02 18:00:07', '2018-09-02 21:00:07', '', 27, 'http://propiedades.test/?post_type=acf-field&#038;p=42', 2, 'acf-field', '', 0),
(44, 1, '2018-09-03 00:19:18', '2018-09-03 03:19:18', '', 'Propiedades', '', 'publish', 'closed', 'closed', '', 'propiedades', '', '', '2018-09-04 02:42:02', '2018-09-04 05:42:02', '', 0, 'http://propiedades.test/?p=44', 3, 'nav_menu_item', '', 0),
(45, 1, '2018-09-04 00:14:22', '2018-09-04 03:14:22', '', 'Residenciales Int', '', 'trash', 'closed', 'closed', '', 'residenciales-2__trashed', '', '', '2018-09-04 00:23:21', '2018-09-04 03:23:21', '', 0, 'http://propiedades.test/?post_type=search-filter-widget&#038;p=45', 0, 'search-filter-widget', '', 0),
(46, 1, '2018-09-04 02:42:02', '2018-09-04 05:42:02', ' ', '', '', 'publish', 'closed', 'closed', '', '46', '', '', '2018-09-04 02:42:02', '2018-09-04 05:42:02', '', 0, 'http://propiedades.test/?p=46', 4, 'nav_menu_item', '', 0),
(47, 1, '2018-09-04 02:42:02', '2018-09-04 05:42:02', ' ', '', '', 'publish', 'closed', 'closed', '', '47', '', '', '2018-09-04 02:42:02', '2018-09-04 05:42:02', '', 0, 'http://propiedades.test/?p=47', 5, 'nav_menu_item', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sin categoría', 'sin-categoria', 0),
(2, 'Main', 'main', 0),
(3, 'Aventura', 'aventura', 0),
(4, 'BAL HARBOUR', 'bal-harbour', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(13, 2, 0),
(14, 2, 0),
(16, 2, 0),
(36, 4, 0),
(39, 3, 0),
(44, 2, 0),
(46, 2, 0),
(47, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 6),
(3, 3, 'barrio', '', 0, 1),
(4, 4, 'barrio', '', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:3:{s:64:\"fe30d118fb3ec4ed968b3f80998878def9af3356d3daa16b97389cf85dc6503d\";a:4:{s:10:\"expiration\";i:1536077146;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535904346;}s:64:\"a26544d7e94e46b5985bfa4be7b624e19e4a4f91297236ca40b46d0467971ed8\";a:4:{s:10:\"expiration\";i:1536077147;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1535904347;}s:64:\"7b6afa11be34a334e602078b5dfbc7862ef57568fc30b958d779fe03efdb93b4\";a:4:{s:10:\"expiration\";i:1536190483;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36\";s:5:\"login\";i:1536017683;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '19'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'show_try_gutenberg_panel', '0'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:3:{i:0;s:25:\"add-post-type-saludunisex\";i:1;s:12:\"add-post_tag\";i:2;s:21:\"add-tipo_de_categoria\";}'),
(22, 1, 'nav_menu_recently_edited', '2'),
(23, 1, 'search-filter-show-welcome-notice', '0'),
(24, 1, 'wp_user-settings', 'editor=tinymce&libraryContent=browse'),
(25, 1, 'wp_user-settings-time', '1535918042'),
(26, 1, 'closedpostboxes_search-filter-widget', 'a:0:{}'),
(27, 1, 'metaboxhidden_search-filter-widget', 'a:2:{i:0;s:23:\"acf-group_5b8c3d934b2cd\";i:1;s:7:\"slugdiv\";}'),
(28, 1, 'closedpostboxes_residenciales', 'a:0:{}'),
(29, 1, 'metaboxhidden_residenciales', 'a:1:{i:0;s:7:\"slugdiv\";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Volcado de datos para la tabla `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B/zBY6pTRES3vbzVXRVxEFC2CXvC4L0', 'admin', 'hernanebaigorria@gmail.com', '', '2018-08-23 02:42:23', '', 0, 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indices de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indices de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indices de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indices de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indices de la tabla `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indices de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indices de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indices de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;
--
-- AUTO_INCREMENT de la tabla `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT de la tabla `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
